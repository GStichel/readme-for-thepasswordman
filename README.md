# **ThePasswordMan**
---

## Überblick
Diese App ermöglicht den Benutzer sein Passwort mit den
meistbenutzten Passwörtern im Internet abzugleichen. Hierbei wird ein Graph anzeigt,
der das Vorkommen von einzelnen Buchstaben des Passworts in der
Datenbank anzeigt. Außerdem können neue Passworter erzeugt werden,
die den vom User ausgewählten Vorgaben entsprechen (Länge, Zeichen usw.).


## Merkmale
* Die Datenbank stammt aus der Open Daten Quelle: (k.A.)  
* Es wird die Häufigkeit von Buchstaben in einem Passwort angezeigt.
* Die von der App erzeugten Passwörter sind, je nach Vorgaben des Benutzers, sehr sicher. 

  

## Installation
Zum Entwickeln sollte Android Studio verwendet werden.
Dazu werden folgende Komponenten benötigt:
 
* Android APK-23
* Gradle 2.14.1
* Ein Emulator oder Android Gerät mit SDK-19 oder höher
  

## Usage
Um ein Passwort zu testen, muss dieses nur in das Eingabefeld eingegeben werden.
Duch ein Klick auf 'find' werden alle Passwörter aus der Datenbank angezeigt, die
das angegebenden Passwört enthalten.  
Die anderen Funktionen sind über das Menu (3 Striche, oben Links) zu erreichen.

* 'Graph' : zeigt die Häufigkeit aller Buchstaben in der Datenbank.
* 'Create': neues Passwort erstellen.  
  
![Hauptscreen.png](https://bitbucket.org/repo/np8zx8/images/1699982965-Unbenannt.png)